#include <berrygl/Application.h>
#include <berrygl/Scene.h>
#include <berrygl/Window.h>
#include <berrygl/Widget.h>
#include <berrygl/math/Math.h>
#include <berrygl/ResourcesManager.h>

class MyScene : public berry::Scene
{
public:
    MyScene ()
        : Scene ()
    {
        berry::Widget* w1 = new berry::Widget ();
        w1->setPos (berry::Vector3::zero ());
        w1->setSize (berry::Vector3::one ());
        w1->setRotation (berry::Vector3::zero ());
        this->addChild (w1);
    }
};

int main (int argc, char* argv[])
{
    std::string         content = berry::ResourcesManager::getInstance ()->load ("res/a.txt");
    float               f       = berry::Mathf::Lerp (100, 200, 0.97F);
    berry::Vector3      v       = berry::Vector3::lerp (berry::Vector3 (100, 100, 100), berry::Vector3 (200, 300, 400), 0.7F);
    berry::Application* app     = berry::Application::instance ();

    app->startup ();
    app->run (new MyScene ());
    app->shutdown ();
}
