#include "berrygl/Widget.h"

berry::Widget::Widget ()
{
    glGenVertexArrays (1, &m_vertex);
    glBindVertexArray (m_vertex);

    GLfloat a[4][3] = {
        -1.0f, -1.0f, 0.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, -1.0f, 0.0f};

    glGenBuffers (4, m_buffers);
    for (int i = 0; i < 4; ++i)
    {
        glBindBuffer (GL_ARRAY_BUFFER, m_buffers[i]);
        glBufferData (GL_ARRAY_BUFFER, 3 * sizeof (GLfloat), a[i], GL_STATIC_DRAW);
    }
}

berry::Widget::~Widget ()
{
}

void berry::Widget::render ()
{
    glBindVertexArray (m_vertex);
    glDrawArrays (GL_TRIANGLES, 0, 4);
}

void berry::Widget::setPos (const berry::Vector3& pos)
{
}

void berry::Widget::setSize (const berry::Vector3& size)
{
}

void berry::Widget::setRotation (const berry::Vector3& rotation)
{
}

const berry::Vector3& berry::Widget::getPos () const
{
    return mPos;
}

const berry::Vector3& berry::Widget::getSize () const
{
    return mSize;
}

const berry::Vector3& berry::Widget::getRotation () const
{
    return mRotation;
}
