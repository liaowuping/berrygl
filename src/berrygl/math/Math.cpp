#include "berrygl/math/Math.h"
#include <cmath>
#include <cfloat>

int berry::Math::max (int a, int b)
{
    if (a > b)
        return a;
    return b;
}

int berry::Math::min (int a, int b)
{
    if (a < b)
        return a;
    return b;
}

berry::Math::Math ()
{
}

berry::Math::~Math ()
{
}

float berry::Mathf::Clamp (float value, float min, float max)
{
    if (value < min)
        value = min;
    if (value > max)
        value = max;
    return value;
}

float berry::Mathf::Lerp (float a, float b, float t)
{
    return ((a * (1 - t)) + (b * t));
}

int berry::Mathf::CeilToInt (float f)
{
    return (int)(f + 1 - FLT_EPSILON);
}

int berry::Mathf::FloorToInt (float f)
{
    return (int)(f);
}

int berry::Mathf::RoundToInt (float f)
{
    return (int)(f + 0.5F);
}

float berry::Mathf::Max (float a, float b)
{
    if (a > b)
        return a;
    return b;
}

float berry::Mathf::Min (float a, float b)
{
    if (a < b)
        return a;
    return b;
}

float berry::Mathf::Cos (float rad)
{
    return cos (rad);
}

float berry::Mathf::Sin (float rad)
{
    return sin (rad);
}

float berry::Mathf::Tan (float rad)
{
    return tan (rad);
}

float berry::Mathf::Sqrt (float rad)
{
    return sqrt (rad);
}

berry::Mathf::Mathf ()
{
}

berry::Mathf::~Mathf ()
{
}
