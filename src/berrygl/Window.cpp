#include "berrygl/Window.h"
#include "berrygl/Scene.h"
#include "berrygl/platform/platform.h"

void onError (int errNum, const char* errDesc)
{
    const char* a = errDesc;
}

berry::Window::Window ()
{
    this->mMainWindow   = nullptr;
    this->mCurrentScene = nullptr;

    glfwSetErrorCallback (onError);

    this->mMainWindow = glfwCreateWindow (480, 270, "sfgsfg", nullptr, nullptr);

    glfwSetWindowAspectRatio (mMainWindow, 1, 1);
    glfwSetMouseButtonCallback (mMainWindow, GLFWEventHandler::onGLFWMouseCallBack);
    glfwSetCursorPosCallback (mMainWindow, GLFWEventHandler::onGLFWMouseMoveCallBack);
    glfwSetScrollCallback (mMainWindow, GLFWEventHandler::onGLFWMouseScrollCallback);
    glfwSetCharCallback (mMainWindow, GLFWEventHandler::onGLFWCharCallback);
    glfwSetKeyCallback (mMainWindow, GLFWEventHandler::onGLFWKeyCallback);
    glfwSetWindowPosCallback (mMainWindow, GLFWEventHandler::onGLFWWindowPosCallback);
    glfwSetFramebufferSizeCallback (mMainWindow, GLFWEventHandler::onGLFWframebuffersize);
    glfwSetWindowSizeCallback (mMainWindow, GLFWEventHandler::onGLFWWindowSizeFunCallback);
    glfwSetWindowIconifyCallback (mMainWindow, GLFWEventHandler::onGLFWWindowIconifyCallback);
    glfwSetWindowFocusCallback (mMainWindow, GLFWEventHandler::onGLFWWindowFocusCallback);

    glfwMakeContextCurrent (this->mMainWindow);
}

berry::Window::~Window ()
{
}

void berry::Window::changeScene (berry::Scene* scene)
{
    this->mCurrentScene = scene;
}

void berry::Window::onEnter ()
{
}

void berry::Window::run ()
{
    glViewport (0, 0, 480, 270);
    while (true)
    {
        /* Swap buffers */
        glfwSwapBuffers (mMainWindow);
        glfwPollEvents ();
        mCurrentScene->onRender ();

        if (glfwWindowShouldClose (mMainWindow) != 0)
        {
            onError (0, "should close");
            break;
        }
    }
}

void berry::Window::onExit ()
{
}

void berry::Window::onGLFWError (int errorID, const char* errorDesc)
{
}

void berry::Window::onGLFWMouseCallBack (GLFWwindow* window, int button, int action, int modify)
{
}

void berry::Window::onGLFWMouseMoveCallBack (GLFWwindow* window, double x, double y)
{
}

void berry::Window::onGLFWMouseScrollCallback (GLFWwindow* window, double x, double y)
{
}

void berry::Window::onGLFWKeyCallback (GLFWwindow* window, int key, int scancode, int action, int mods)
{
}

void berry::Window::onGLFWCharCallback (GLFWwindow* window, unsigned int character)
{
}

void berry::Window::onGLFWWindowPosCallback (GLFWwindow* windows, int x, int y)
{
}

void berry::Window::onGLFWframebuffersize (GLFWwindow* window, int w, int h)
{
}

void berry::Window::onGLFWWindowSizeFunCallback (GLFWwindow* window, int width, int height)
{
}

void berry::Window::onGLFWWindowIconifyCallback (GLFWwindow* window, int iconified)
{
}

void berry::Window::onGLFWWindowFocusCallback (GLFWwindow* window, int focused)
{
}
