#include "berrygl/Vector3.h"
#include "berrygl/math/Math.h"

berry::Vector3 berry::Vector3::zero ()
{
    return berry::Vector3 (0, 0, 0);
}

berry::Vector3 berry::Vector3::one ()
{
    return berry::Vector3 (1, 1, 1);
}

berry::Vector3 berry::Vector3::left ()
{
    return berry::Vector3 (-1, 0, 0);
}

berry::Vector3 berry::Vector3::right ()
{
    return berry::Vector3 (1, 0, 0);
}

berry::Vector3 berry::Vector3::up ()
{
    return berry::Vector3 (0, 1, 0);
}

berry::Vector3 berry::Vector3::down ()
{
    return berry::Vector3 (0, -1, 0);
}

berry::Vector3 berry::Vector3::forward ()
{
    return berry::Vector3 (0, 0, 1);
}

berry::Vector3 berry::Vector3::back ()
{
    return berry::Vector3 (0, 0, -1);
}

float berry::Vector3::distance (const berry::Vector3& a, const berry::Vector3& b)
{
    return Mathf::Sqrt ((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y) + (a.z - b.z) * (a.z - b.z));
}

float berry::Vector3::dot (const berry::Vector3& a, const berry::Vector3& b)
{
    return ((a.x * b.x) + (a.y * b.y) + (a.z * b.z));
}

berry::Vector3 berry::Vector3::corss (const berry::Vector3& a, const berry::Vector3& b)
{
    return berry::Vector3 ((a.y * b.z - b.y * a.z), (a.z * b.x - b.z * a.x), (a.x * b.y - b.x * a.y));
}

berry::Vector3 berry::Vector3::lerp (const berry::Vector3& a, const berry::Vector3& b, float t)
{
    return ((a * (1 - t)) + (b * t));
}

berry::Vector3::Vector3 ()
{
    this->x = 0;
    this->y = 0;
    this->z = 0;
}

berry::Vector3::Vector3 (const berry::Vector3& v3)
{
    this->x = v3.x;
    this->y = v3.y;
    this->z = v3.z;
}

berry::Vector3::Vector3 (float x, float y, float z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

berry::Vector3::~Vector3 ()
{
}

berry::Vector3 berry::operator+ (const berry::Vector3& a, const berry::Vector3& b)
{
    return berry::Vector3 (a.x + b.x, a.y + b.y, a.z + b.z);
}

berry::Vector3 berry::operator- (const berry::Vector3& a, const berry::Vector3& b)
{
    return berry::Vector3 (a.x - b.x, a.y - b.y, a.z - b.z);
}

berry::Vector3 berry::operator- (const berry::Vector3& a)
{
    return berry::Vector3 (-a.x, -a.y, -a.z);
}

berry::Vector3 berry::operator* (const berry::Vector3& a, float f)
{
    return berry::Vector3 (a.x * f, a.y * f, a.z * f);
}

berry::Vector3 berry::operator* (float f, const berry::Vector3& a)
{
    return berry::Vector3 (f * a.x, f * a.y, f * a.z);
}

berry::Vector3 berry::operator/ (const berry::Vector3& a, float f)
{
    return berry::Vector3 (a.x / f, a.y / f, a.z / f);
}

bool berry::operator== (const berry::Vector3& a, const berry::Vector3& b)
{
    return ((a.x == b.x) && (a.y == b.y) && (a.z == b.z));
}

bool berry::operator!= (const berry::Vector3& a, const berry::Vector3& b)
{
    return ((a.x != b.x) || (a.y != b.y) || (a.z != b.z));
}
