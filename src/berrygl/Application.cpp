#include "berrygl/Application.h"
#include "berrygl/Window.h"
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include <stdio.h>

berry::Application* berry::Application::instance ()
{
    if (_inst == nullptr)
    {
        _inst = new berry::Application ();
    }
    return _inst;
}

berry::Application::Application ()
{
}

berry::Application::~Application ()
{
}

void berry::Application::startup ()
{
    glfwInit ();

    mCurrentWindow = new berry::Window ();

    glewInit ();
}

void berry::Application::shutdown ()
{
    glfwTerminate ();
}

void berry::Application::run (berry::Scene* scene)
{
    mCurrentWindow->changeScene (scene);
    mCurrentWindow->onEnter ();
    mCurrentWindow->run ();
    mCurrentWindow->onExit ();
}

berry::Application* berry::Application::_inst = nullptr;
