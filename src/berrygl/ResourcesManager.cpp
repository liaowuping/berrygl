#include "berrygl/ResourcesManager.h"
#include "PathDef.h"
#include <iostream>
#include <fstream> // 文件流
#include <sstream>

berry::ResourcesManager::ResourcesManager ()
{
}

berry::ResourcesManager::~ResourcesManager ()
{
}

berry::ResourcesManager* berry::ResourcesManager::getInstance ()
{
    if (m_inst == nullptr)
    {
        m_inst = new berry::ResourcesManager ();
    }
    return m_inst;
}

std::string berry::ResourcesManager::load (const std::string& path)
{
    std::string        absolutePath = std::string (PROJECT_ROOT) + path; // 获取真实路径.
    std::ifstream      file (absolutePath, std::ios::in);                // 打开文件
    std::ostringstream s;
    s << file.rdbuf ();
    return s.str ();
}

berry::ResourcesManager* berry::ResourcesManager::m_inst = nullptr;
