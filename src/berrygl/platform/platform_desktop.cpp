#include "berrygl/platform/platform_desktop.h"
#include "berrygl/Window.h"

void berry::GLFWEventHandler::setGLViewImpl (berry::Window* view)
{
    _currentCanvas = view;
}

void berry::GLFWEventHandler::onGLFWError (int errorID, const char* errorDesc)
{
    if (_currentCanvas)
        _currentCanvas->onGLFWError (errorID, errorDesc);
}

void berry::GLFWEventHandler::onGLFWMouseCallBack (GLFWwindow* window, int button, int action, int modify)
{
    if (_currentCanvas)
        _currentCanvas->onGLFWMouseCallBack (window, button, action, modify);
}

void berry::GLFWEventHandler::onGLFWMouseMoveCallBack (GLFWwindow* window, double x, double y)
{
    if (_currentCanvas)
        _currentCanvas->onGLFWMouseMoveCallBack (window, x, y);
}

void berry::GLFWEventHandler::onGLFWMouseScrollCallback (GLFWwindow* window, double x, double y)
{
    if (_currentCanvas)
        _currentCanvas->onGLFWMouseScrollCallback (window, x, y);
}

void berry::GLFWEventHandler::onGLFWKeyCallback (GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (_currentCanvas)
        _currentCanvas->onGLFWKeyCallback (window, key, scancode, action, mods);
}

void berry::GLFWEventHandler::onGLFWCharCallback (GLFWwindow* window, unsigned int character)
{
    if (_currentCanvas)
        _currentCanvas->onGLFWCharCallback (window, character);
}

void berry::GLFWEventHandler::onGLFWWindowPosCallback (GLFWwindow* windows, int x, int y)
{
    if (_currentCanvas)
        _currentCanvas->onGLFWWindowPosCallback (windows, x, y);
}

void berry::GLFWEventHandler::onGLFWframebuffersize (GLFWwindow* window, int w, int h)
{
    if (_currentCanvas)
        _currentCanvas->onGLFWframebuffersize (window, w, h);
}

void berry::GLFWEventHandler::onGLFWWindowSizeFunCallback (GLFWwindow* window, int width, int height)
{
    if (_currentCanvas)
        _currentCanvas->onGLFWWindowSizeFunCallback (window, width, height);
}

void berry::GLFWEventHandler::onGLFWWindowIconifyCallback (GLFWwindow* window, int iconified)
{
    if (_currentCanvas)
    {
        _currentCanvas->onGLFWWindowIconifyCallback (window, iconified);
    }
}

void berry::GLFWEventHandler::onGLFWWindowFocusCallback (GLFWwindow* window, int focused)
{
    if (_currentCanvas)
    {
        _currentCanvas->onGLFWWindowFocusCallback (window, focused);
    }
}

berry::Window* berry::GLFWEventHandler::_currentCanvas;
