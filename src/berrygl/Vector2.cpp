#include "berrygl/Vector2.h"

berry::Vector2 berry::Vector2::zero ()
{
    return berry::Vector2 (0, 0);
}

berry::Vector2 berry::Vector2::one ()
{
    return berry::Vector2 (1, 1);
}

berry::Vector2::Vector2 ()
{
    this->x = 0;
    this->y = 0;
}

berry::Vector2::Vector2 (const berry::Vector2& v2)
{
    this->x = v2.x;
    this->y = v2.y;
}

berry::Vector2::Vector2 (float x, float y)
{
    this->x = x;
    this->y = y;
}

berry::Vector2::~Vector2 ()
{
}
