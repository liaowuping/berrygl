#include "berrygl/Vector4.h"

berry::Vector4::Vector4 ()
{
    this->x = 0;
    this->y = 0;
    this->z = 0;
    this->w = 0;
}

berry::Vector4::Vector4 (const berry::Vector4& v4)
{
    this->x = v4.x;
    this->y = v4.y;
    this->z = v4.z;
    this->w = v4.w;
}

berry::Vector4::Vector4 (float x, float y, float z, float w)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->w = w;
}

berry::Vector4::~Vector4 ()
{
}
