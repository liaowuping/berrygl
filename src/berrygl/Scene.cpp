#include "berrygl/Scene.h"
#include "berrygl/Widget.h"
#include "berrygl/platform/platform.h"

berry::Scene::Scene ()
{

    this->mChildren = new std::vector<berry::Widget*> ();
}

berry::Scene::~Scene ()
{
}

void berry::Scene::addChild (berry::Widget* child)
{
    mChildren->push_back (child);
}

void berry::Scene::removeChild (berry::Widget* child)
{
    for (auto it = mChildren->begin (); it != mChildren->end ();)
    {
        if (*it == child)
            it = mChildren->erase (it);
        else
            ++it;
    }
}

void berry::Scene::onEnter ()
{
}

void berry::Scene::onRender ()
{
    for (auto child : *mChildren)
    {
        child->render ();
    }
}

void berry::Scene::onExit ()
{
}
