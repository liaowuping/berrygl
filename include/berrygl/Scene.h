#ifndef Canvas_H
#define Canvas_H

#include "berrygl/Ref.h"
#include <vector>

namespace berry
{
    class Widget;

    class BERRY_DLL Scene : public Ref
    {
    public:
        Scene ();
        ~Scene ();

        void addChild (berry::Widget* child);

        void removeChild (berry::Widget* child);

        virtual void onEnter ();

        virtual void onRender ();

        virtual void onExit ();

    private:
        std::vector<berry::Widget*>* mChildren;
    };
}

#endif
