#ifndef Vector3_H
#define Vector3_H

#include "berrygl/Ref.h"

namespace berry
{
    class BERRY_DLL Vector3 : public berry::Ref
    {
    public:
        static berry::Vector3 zero ();

        static berry::Vector3 one ();

        static berry::Vector3 left ();

        static berry::Vector3 right ();

        static berry::Vector3 up ();

        static berry::Vector3 down ();

        static berry::Vector3 forward ();

        static berry::Vector3 back ();

        static float distance (const berry::Vector3& a, const berry::Vector3& b);

        static float dot (const berry::Vector3& a, const berry::Vector3& b);

        static berry::Vector3 corss (const berry::Vector3& a, const berry::Vector3& b);

        static berry::Vector3 lerp (const berry::Vector3& a, const berry::Vector3& b, float t);

    public:
        Vector3 ();

        Vector3 (const berry::Vector3& v3);

        Vector3 (float x, float y, float z);

        ~Vector3 ();

    public:
        float x;

        float y;

        float z;
    };

    static berry::Vector3 operator+ (const berry::Vector3& a, const berry::Vector3& b);

    static berry::Vector3 operator- (const berry::Vector3& a, const berry::Vector3& b);

    static berry::Vector3 operator- (const berry::Vector3& a);

    static berry::Vector3 operator* (const berry::Vector3& a, float f);

    static berry::Vector3 operator* (float f, const berry::Vector3& a);

    static berry::Vector3 operator/ (const berry::Vector3& a, float f);

    static bool operator== (const berry::Vector3& a, const berry::Vector3& b);

    static bool operator!= (const berry::Vector3& a, const berry::Vector3& b);
}

#endif
