#ifndef Rect_H
#define Rect_H

#include "berrygl/Ref.h"

namespace berry
{
    class BERRY_DLL Rect : public berry::Ref
    {
    public:
        Rect (int x, int y, int w, int h);

        Rect (const berry::Rect& rc);

        ~Rect ();
    };
}

#endif
