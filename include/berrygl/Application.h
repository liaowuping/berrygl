#ifndef Application_H
#define Application_H

#include "berrygl/Ref.h"

namespace berry
{
    class Window;
    class Scene;

    class BERRY_DLL Application : public berry::Ref
    {
    public:
        static Application* instance ();

        Application ();
        ~Application ();

        void startup ();

        void shutdown ();

        void run (berry::Scene* scene);

    private:
        static Application* _inst;

        berry::Window* mCurrentWindow;
    };
}

#endif
