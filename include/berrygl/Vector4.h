#ifndef Vector4_H
#define Vector4_H

#include "berrygl/Ref.h"

namespace berry
{
    class BERRY_DLL Vector4 : public berry::Ref
    {
    public:
        Vector4 ();

        Vector4 (const berry::Vector4& v4);

        Vector4 (float x, float y, float z, float w);

        ~Vector4 ();

    public:
        float x;

        float y;

        float z;

        float w;
    };
}

#endif
