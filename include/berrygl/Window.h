#ifndef Window_H
#define Window_H

#include "berrygl/Ref.h"

struct GLFWwindow;

namespace berry
{
    class Scene;

    class Window : public berry::Ref
    {
    public:
        Window ();

        ~Window ();

        virtual void changeScene (berry::Scene* scene);

        virtual void onEnter ();

        virtual void run ();

        virtual void onExit ();

        virtual void onGLFWError (int errorID, const char* errorDesc);

        virtual void onGLFWMouseCallBack (GLFWwindow* window, int button, int action, int modify);

        virtual void onGLFWMouseMoveCallBack (GLFWwindow* window, double x, double y);

        virtual void onGLFWMouseScrollCallback (GLFWwindow* window, double x, double y);

        virtual void onGLFWKeyCallback (GLFWwindow* window, int key, int scancode, int action, int mods);

        virtual void onGLFWCharCallback (GLFWwindow* window, unsigned int character);

        virtual void onGLFWWindowPosCallback (GLFWwindow* windows, int x, int y);

        virtual void onGLFWframebuffersize (GLFWwindow* window, int w, int h);

        virtual void onGLFWWindowSizeFunCallback (GLFWwindow* window, int width, int height);

        virtual void onGLFWWindowIconifyCallback (GLFWwindow* window, int iconified);

        virtual void onGLFWWindowFocusCallback (GLFWwindow* window, int focused);

    protected:
        berry::Scene* mCurrentScene;

    private:
        GLFWwindow* mMainWindow;
    };
}
#endif
