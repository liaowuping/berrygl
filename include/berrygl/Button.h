#ifndef Button_H
#define Button_H

#include "berrygl/Widget.h"

namespace berry
{
    class BERRY_DLL Button : public berry::Widget
    {
    public:
        Button ();
        ~Button ();
    };
}

#endif
