#ifndef Vector2_H
#define Vector2_H

#include "berrygl/Ref.h"

namespace berry
{
    class BERRY_DLL Vector2 : public berry::Ref
    {
    public:
        static berry::Vector2 zero ();

        static berry::Vector2 one ();

    public:
        Vector2 ();

        Vector2 (const berry::Vector2& v2);

        Vector2 (float x, float y);

        ~Vector2 ();

    public:
        float x;

        float y;
    };
}

#endif
