#ifndef ResourcesManager_H
#define ResourcesManager_H

#include <string>

namespace berry
{
    class ResourcesManager
    {
    private:
        ResourcesManager ();

        ~ResourcesManager ();

    public:
        static ResourcesManager* getInstance ();

        std::string load (const std::string& path);

    private:
        static ResourcesManager* m_inst;
    };
}

#endif
