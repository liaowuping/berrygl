#ifndef Widget_H
#define Widget_H

#include "berrygl/Ref.h"
#include "berrygl/Vector3.h"

namespace berry
{
    class BERRY_DLL Widget : public berry::Ref
    {
    public:
        Widget ();

        ~Widget ();

        void render ();

        void setPos (const berry::Vector3& pos);

        void setSize (const berry::Vector3& size);

        void setRotation (const berry::Vector3& rotation);

        const berry::Vector3& getPos () const;

        const berry::Vector3& getSize () const;

        const berry::Vector3& getRotation () const;

    private:
        berry::Vector3 mSize;

        berry::Vector3 mPos;

        berry::Vector3 mRotation;

        GLuint m_vertex;
        GLuint m_buffers[4];
    };
}

#endif
