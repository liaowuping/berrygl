#ifndef Math_H
#define Math_H

#include "berrygl/Ref.h"

namespace berry
{
    class BERRY_DLL Math
    {
    public:
        static int max (int a, int b);

        static int min (int a, int b);

    private:
        Math ();

        ~Math ();
    };

    class BERRY_DLL Mathf
    {
    public:
        static float Clamp (float value, float min, float max);

        static float Lerp (float a, float b, float t);

        static int CeilToInt (float f);

        static int FloorToInt (float f);

        static int RoundToInt (float f);

        static float Max (float a, float b);

        static float Min (float a, float b);

        static float Cos (float rad);

        static float Sin (float rad);

        static float Tan (float rad);

        static float Sqrt (float rad);

        const float PI = 3.1415927F;

        const float Deg2Rad = 0.01745329F;

        const float Rad2Deg = 57.29578F;

    private:
        Mathf ();

        ~Mathf ();
    };
}
#endif
