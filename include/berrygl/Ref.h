#ifndef Ref_H
#define Ref_H

#include "berrygl/platform/platform.h"

namespace berry
{
    class BERRY_DLL Ref
    {
    public:
        Ref ();
        ~Ref ();
    };
}

#endif
