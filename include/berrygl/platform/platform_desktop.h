#ifndef platform_desktop_H
#define platform_desktop_H

#include "GL/glew.h"
#include "GLFW/glfw3.h"

struct GLFWwindow;

namespace berry
{
    class Window;

    class GLFWEventHandler
    {
    public:
        static void setGLViewImpl (berry::Window* view);

        static void onGLFWError (int errorID, const char* errorDesc);

        static void onGLFWMouseCallBack (GLFWwindow* window, int button, int action, int modify);

        static void onGLFWMouseMoveCallBack (GLFWwindow* window, double x, double y);

        static void onGLFWMouseScrollCallback (GLFWwindow* window, double x, double y);

        static void onGLFWKeyCallback (GLFWwindow* window, int key, int scancode, int action, int mods);

        static void onGLFWCharCallback (GLFWwindow* window, unsigned int character);

        static void onGLFWWindowPosCallback (GLFWwindow* windows, int x, int y);

        static void onGLFWframebuffersize (GLFWwindow* window, int w, int h);

        static void onGLFWWindowSizeFunCallback (GLFWwindow* window, int width, int height);

        static void onGLFWWindowIconifyCallback (GLFWwindow* window, int iconified);

        static void onGLFWWindowFocusCallback (GLFWwindow* window, int focused);

    private:
        static berry::Window* _currentCanvas;
    };
}

#endif
