#ifndef platform_H
#define platform_H

#ifdef WIN32

#define BERRY_DLL __declspec(dllexport)

#else

#define BERRY_DLL

#endif

/**
 * 暂时先只支持桌面平台,移动平台暂时先不做
 **/
#include "berrygl/platform/platform_desktop.h"

#endif
