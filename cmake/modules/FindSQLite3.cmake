# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#.rst:
# FindSQLITE3
# --------
#
# Find the OpenGL Extension Wrangler Library (SQLITE3)
#
# IMPORTED Targets
# ^^^^^^^^^^^^^^^^
#
# This module defines the :prop_tgt:`IMPORTED` target ``SQLITE3::SQLITE3``,
# if SQLITE3 has been found.
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# This module defines the following variables:
#
# ::
#
#   SQLITE3_INCLUDE_DIRS - include directories for SQLITE3
#   SQLITE3_LIBRARIES - libraries to link against SQLITE3
#   SQLITE3_FOUND - true if SQLITE3 has been found and can be used

find_path(SQLITE3_INCLUDE_DIR sqlite3.h)

if(NOT SQLITE3_LIBRARY)
  find_library(SQLITE3_LIBRARY_RELEASE NAMES SQLITE3 sqlite3 PATH_SUFFIXES lib64)
  find_library(SQLITE3_LIBRARY_DEBUG NAMES sqlite3 PATH_SUFFIXES lib64)

  include(${CMAKE_CURRENT_LIST_DIR}/SelectLibraryConfigurations.cmake)
  select_library_configurations(SQLITE3)
endif ()

include(${CMAKE_CURRENT_LIST_DIR}/FindPackageHandleStandardArgs.cmake)
find_package_handle_standard_args(SQLITE3
                                  REQUIRED_VARS SQLITE3_INCLUDE_DIR SQLITE3_LIBRARY)

if(SQLITE3_FOUND)
  set(SQLITE3_INCLUDE_DIRS ${SQLITE3_INCLUDE_DIR})

  if(NOT SQLITE3_LIBRARIES)
    set(SQLITE3_LIBRARIES ${SQLITE3_LIBRARY})
  endif()

  if (NOT TARGET SQLITE3::SQLITE3)
    add_library(SQLITE3::SQLITE3 UNKNOWN IMPORTED)
    set_target_properties(SQLITE3::SQLITE3 PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${SQLITE3_INCLUDE_DIRS}")

    if(SQLITE3_LIBRARY_RELEASE)
      set_property(TARGET SQLITE3::SQLITE3 APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
      set_target_properties(SQLITE3::SQLITE3 PROPERTIES IMPORTED_LOCATION_RELEASE "${SQLITE3_LIBRARY_RELEASE}")
    endif()

    if(SQLITE3_LIBRARY_DEBUG)
      set_property(TARGET SQLITE3::SQLITE3 APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
      set_target_properties(SQLITE3::SQLITE3 PROPERTIES IMPORTED_LOCATION_DEBUG "${SQLITE3_LIBRARY_DEBUG}")
    endif()

    if(NOT SQLITE3_LIBRARY_RELEASE AND NOT SQLITE3_LIBRARY_DEBUG)
      set_property(TARGET SQLITE3::SQLITE3 APPEND PROPERTY IMPORTED_LOCATION "${SQLITE3_LIBRARY}")
    endif()
  endif()
endif()

mark_as_advanced(SQLITE3_INCLUDE_DIR)
